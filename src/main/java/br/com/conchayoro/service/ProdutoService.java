package br.com.conchayoro.service;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;

import br.com.conchayoro.entity.Produto;
import br.com.conchayoro.persistence.ProdutoDao;

@Path("/produtos")
@RequestScoped
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})	
public class ProdutoService {

	@Inject
	private ProdutoDao produtoDao;
	
	@Inject
	private Logger log;
	
	Produto entity;
	
	@Path("/incluir")
	@POST	
	public String incluir(Produto produto){

		entity = new Produto();
		entity.setNome(produto.getNome());
		entity.setUnidade(produto.getUnidade());
		entity.setPrecoUnitario(produto.getPrecoUnitario());

    	produtoDao.incluir(entity);
    	
    	log.debug("produtoDao.incluir():"+entity);
		
		return "Produto incluído com sucesso!";

	}

	@Path("/alterar")
	@PUT
	public String alterar(Produto produto){

		entity = new Produto();
		entity.setId(produto.getId());
		entity.setNome(produto.getNome());
		entity.setUnidade(produto.getUnidade());
		entity.setPrecoUnitario(produto.getPrecoUnitario());

		produtoDao.alterar(entity);
		log.debug("produtoDao.alterar():"+entity);
		
		return "Produto alterado com sucesso!";

	}
	
	@Path("/listar")
	@GET
	public List<Produto> listar(){
		
		return produtoDao.listar();
	}

	@Path("/excluir/{id}")
	@DELETE
	public String excluir(@PathParam("id") Long id){

		produtoDao.excluir(id);
		log.debug("produtoDao.excluir():"+id);
		
		return "Produto excluído com sucesso!";

	}

	@Path("/consultar/{id}")
	@GET
	public Produto consultar(@PathParam("id") Long id){

		entity = produtoDao.consultar(id);

		return entity;
	}
	
}